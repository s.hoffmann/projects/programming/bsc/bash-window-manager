#!/bin/bash

VERSION="1.0.1"

SCRIPT_DIR="$(dirname "$(realpath "$0")")"
LOG_FILE="$SCRIPT_DIR/log.out"
FOUND_WINDOW_ID=""


# Prints the help text
# param: -
# return: usage on stdout
function show_help {
  (echo "
  Usage: Personal Window Manager

  The user links this file in his custom hotkey settings. There it is possible to set this
  script as a command to be executed once a specified shortcut was typed. The script awaits
  one of two possibilities as a parameter:

  * -v | --version: shows version number
  * -h | --help:    show this help
  * -r | --reset:   reset all previous settings
  * -s | --set MEMORY_SLOT:
                    set current window to the specified memory slot
  * -c | --command EXECUTION_COMMAND:
                    Specifies which command should get executed in a background bash
                    session. Typically this would include something like 'code' in order
                    to open a new instance of vs code.

  Once a command that opens a new ui window, this window gets appended its startup command
  using the xprop utilities. For this the 'MY_NAME' Field of a given window instance was used.
  Once the user wants to select this window again, he just executes the given command and the
  program searches the available windows for the specified tag. If nothing was found, the
  startup command will get executed again, otherwise the window with the startup command in its
  hypertags gets focussed.

  It is also possible to set a already opened window to open memory slots using the set
  function. Here no startup command gets executed and the specified memory slot is used as
  search parameter.
")
}

# Toggles visibility of window with the given id
# params: window_id: decimal window id
# return: -
function toggle_visibility {
  window_id=$1
  echo "toggle window_id: $window_id" >>$LOG_FILE

  active_window_id=$(xdotool getactivewindow)
  echo "win id: $active_window_id" >>$LOG_FILE
  if [[ "$active_window_id" == "$window_id" ]]; then
    echo "minimize window: $window_id" >>$LOG_FILE
    xdotool windowminimize $window_id
  else
    echo "activate window: $window_id" >>$LOG_FILE
    xdotool windowactivate $window_id
  fi

}

# Iterates through the available windows to find the window with
# param: window tag name (startup command when started via hotkey, otherwise it's the name of the mem slot
# return: -
function find_win_by_name {
  window_name=$1
  echo "finding window with MY_NAME = $window_name" >>$LOG_FILE
#  window_ids=$(xdotool search --name "")
  window_ids=$(wmctrl -l | tac | awk '{print $1}')
  for window_id in $window_ids; do
      echo "checking id: $window_id" >>$LOG_FILE
      xprop_output=$(xprop -id $window_id MY_NAME 2>/dev/null)
      if [[ $xprop_output == *"$tag_name"* ]]; then
          echo "Found MY_NAME='$tag_name' on window $window_id" >>$LOG_FILE
          # convert from hex to dec - xdotool needs decs for minimizing window (activate works with hex)
          FOUND_WINDOW_ID=$(($window_id))
          break
      fi
  done
}

# Executes the startup command and waits until the window spawned
# param: startup command
# return: -
function execute_startup_command {
  startup_command=$1
  echo "No window found with MY_NAME='$startup_command' execute startup command" >>$LOG_FILE
  old_window_count=$(wmctrl -l | wc -l)
  max_iterations=10
  i=0

  echo "nohup $startup_command > /dev/null 2>&1 &" >>$LOG_FILE
  nohup $startup_command > /dev/null 2>&1 &

  while [[ $i -lt $max_iterations ]]; do
      sleep .5  # Adjust sleep duration if needed

      new_window_count=$(wmctrl -l | wc -l)

      if [[ $new_window_count -ne $old_window_count ]]; then
          echo "Window count changed from $old_window_count to $new_window_count. Exiting loop." >>$LOG_FILE
          break
      fi

      ((i++))
  done

  wmctrl -l >>$LOG_FILE
}

# Indicates to the user that the tagging process after spawning a new ui window
# is finished.
# params: -
# return: -
function hint_tagging_finished {
  active_window_id=$(xdotool getactivewindow)
  toggle_visibility $active_window_id
  sleep .5
  toggle_visibility $active_window_id
}

# Tags the last created window. When listing the wmctrl windows the last entry
# represents the last opened window
# params: -
# return: -
function tag_new_window {
  active_window_id=$(wmctrl -l | tail -n 1 | awk '{print $1}')
  echo "- new window to tag $active_window_id" >>$LOG_FILE
  xprop_out=$(xprop -id $active_window_id MY_NAME)
  echo "- xprop out: $xprop_out" >>$LOG_FILE
  if echo $xprop_out | grep -q "not found" || echo $xprop_out | grep -q "no such atom"; then
    xprop -id $active_window_id -format MY_NAME 8u -set MY_NAME "$tag_name"
    xdotool windowactivate "$active_window_id"
    hint_tagging_finished
  fi
}

# Sets the tag name for the current window. Used for setting already opened window
# to memory slot.
# params: -
# return: -
function tag_current_window {
  active_window_id=$(xdotool getactivewindow)
  echo "- current window to tag $active_window_id" >>$LOG_FILE
  xprop_out=$(xprop -id $active_window_id MY_NAME)
  echo "- xprop out: $xprop_out" >>$LOG_FILE
  if echo $xprop_out | grep -q "not found" || echo $xprop_out | grep -q "no such atom"; then
    xprop -id $active_window_id -format MY_NAME 8u -set MY_NAME "$tag_name"
    xdotool windowactivate "$active_window_id"
    hint_tagging_finished
  fi
}

# Reset the tag 'MY_NAME' using the xprop utilities for the given window id
# params: window id
# return: -
function reset_win_with_name {
  window_id=$1
  echo "reset MY_NAME prop of window with id = $window_id" >>$LOG_FILE
  xprop -id $window_id -format MY_NAME 8u -set MY_NAME ""
}

# Iterates through all available windows and resets the startup command tag
# on each of them.
# params: -
# return: -
function reset_all_window_names {
  echo 'reset all window names' >>$LOG_FILE
  window_ids=$(xdotool search --name "")
  for window_id in $window_ids; do
    reset_win_with_name $window_id
  done
}


mode=$1
tag_name=$2

if [[ "$#" -lt 1 || "$#" -gt 2 || "$mode" == "-h" || "$mode" == "--help" || \
  ( "$mode" != "-c" && "$mode" != "--command" \
  && "$mode" != "-s" && "$mode" != "--set" \
  && "$mode" != "-r" && "$mode" != "--reset" \
  && "$mode" != "-v" && "$mode" != "--version") ]]; then
  show_help
  exit 1
elif [[ "$mode" == "-v" || "$mode" == "--version" ]]; then
  echo "bash memory window manager - version $VERSION"
  echo "bash memory window manager - version $VERSION" >>$LOG_FILE
  exit 0
elif [[ "$mode" == "-r" || "$mode" == "--reset" ]]; then
  echo "reset parameter '-r' set" >>$LOG_FILE
  reset_all_window_names
  echo '------------------------' >>$LOG_FILE
  exit 0
fi

echo "tag name / startup command: $tag_name"

find_win_by_name "$tag_name"
output_win_id=$FOUND_WINDOW_ID

if [[ -n "$output_win_id" ]]; then
  # window tagged with given startup command already present -> toggle visibility
  echo "Selected window ID: $output_win_id" >>$LOG_FILE
  toggle_visibility $output_win_id
else

  if [[ "$mode" == "-c" || "$mode" == "--command" ]]; then
    # window not present -> use startup command and tag newly created window
    execute_startup_command "$tag_name"
    tag_new_window
  else
    tag_current_window
  fi
fi

echo '++++++++++++++++++++++++' >>$LOG_FILE

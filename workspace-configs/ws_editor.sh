#!/bin/bash

SCRIPT_DIR="$(dirname "$(realpath "$0")")"
LOG_FILE="$SCRIPT_DIR/log.out"

source /etc/environment

if [ -n "$CUSTOM_CURRENT_EDITOR_NAME" ]; then
  echo "opening $CUSTOM_CURRENT_EDITOR_NAME"
  $CUSTOM_CURRENT_EDITOR_NAME &
else
  echo 'error :: no value set for env CUSTOM_CURRENT_EDITOR_NAME, please set it in /etc/environment'
fi

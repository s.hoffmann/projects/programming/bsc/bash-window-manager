# Bash Window Manager

Utility that provides hotkey navigation between opened windows.

## Configuration

- Clone repo and copy the complete path of the script
- Go to the keyboard settings page and create a new custom shortcut e.g. `alt + 1`
- In the command section of the setting enter the previously copied script path and provide any unique
  String as the memory slot identifier as a parameter for the script in the command section
- Save the settings
- Set the appropriate key (which was used in the settings as a parameter) in the [config](./config.env) script
- Create a workspace config for given key, just copy the terminal command into a script in [this](workspace-configs)
  location.

## Usage

- Press the configured shortcut to access the memory slot for the configured window
- Navigate to any other window and press the shortcut, now the previously configured window should open
- To toggle the view simply press the shortcut again
- To reset the memory slots, configure another custom shortcut that takes in the letter 'r' as its argument

```bash
$ ./script/location/window-memory-manager.sh -h

  Usage: Personal Window Manager

  The user links this file in his custom hotkey settings. There it is possible to set this
  script as a command to be executed once a specified shortcut was typed. The script awaits
  one of two possibilities as a parameter:

  * -v | --version: shows version number
  * -h | --help:    show this help
  * -r | --reset:   reset all previous settings
  * -s | --set MEMORY_SLOT:
                    set current window to the specified memory slot
  * -c | --command EXECUTION_COMMAND:
                    Specifies which command should get executed in a background bash
                    session. Typically this would include something like 'code' in order
                    to open a new instance of vs code.

  Once a command that opens a new ui window, this window gets appended its startup command
  using the xprop utilities. For this the 'MY_NAME' Field of a given window instance was used.
  Once the user wants to select this window again, he just executes the given command and the
  program searches the available windows for the specified tag. If nothing was found, the
  startup command will get executed again, otherwise the window with the startup command in its
  hypertags gets focussed.

  It is also possible to set a already opened window to open memory slots using the set
  function. Here no startup command gets executed and the specified memory slot is used as
  search parameter.
```

## Todo

- [ ] implement a quick blink animation when window is changed
- [ ] insert usage demo gif into readme
